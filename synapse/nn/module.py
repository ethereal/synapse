# Copyright 2023 Ethereal.
#
# This software is licensed under the MIT license. See LICENSE for details.


"""Implements modules."""


from synapse.variable import Variable


class Module:
    """Implmenents a module.

    Modules are used to group together related functions and variables
    within a computational graph.

    Attributes
    ----------

    variables:
       An iterator containing all variables used by the module.

    Methods
    -------

    forward: Performs a forward pass.
    """

    def forward(self) -> Variable:
        """Performs a forward pass.

        Returns
        -------

        variable: The output of the module.

        Raises
        ------

        NotImplementedError: Raised unless this method is overloaded.
        """

        raise NotImplementedError()


    # Make the module callable.

    __call__ = forward


    @property
    def variables(self) -> Iterator:
        """An iterator containing all variables used by the module."""

        for variable in self.__dict__.values():
            if isinstance(variable, Module):
                yield from variable.variables

            if isinstance(variable, Variable):
                if variable.leaf:
                    yield variable