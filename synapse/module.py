# Copyright (c) 2023 ethereal.
#
# This software is licensed under the MIT license. See LICENSE for details.


"""Synapse modules."""


from typing import Iterator
from synapse.variable import Variable


class Module:
    """Defines a module.

    Modules are used to manipulate collections of variables.

    Attributes
    ----------

    variables: An iterator containing the variables used by the module.
    """

    @property
    def variables(self) -> Iterator[Variable]:
        """An iterator containing the variables used by the module."""

        for variable in self.__dict__.values():
            if isinstance(variable, Module):
                yield from variable.variables

            if isinstance(variable, Variable):
                if variable.external:
                    yield variable