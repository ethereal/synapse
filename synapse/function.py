# Copyright 2023 Ethereal.
#
# This software is licensed under the MIT license. See LICENSE for details.


"""Implements functions in computational graphs."""


from synapse.variable import Variable


class Function:
    """Implements a function.

    In computational graphs, functions are used to represent computations
    performed on variables.

    Methods
    -------

    forward: Takes in several input variables and returns an output variable.

    backward: Updates the gradient of each input variable.
    """

    def forward(self, *_) -> Variable:
        """Takes in several input variables and returns an output variable.

        Parameters
        ----------

        *variables: Several input variables.

        Returns
        -------

        variable: The ouput variable.

        Raises
        ------

        NotImplementedError: Raised unless this method is overloaded.
        """

        raise NotImplementedError()


    def backward(self, *_, __) -> None:
        """Updates the gradient of each input variable.

        Parameters
        ----------

        *variables: Several input variables.

        grad: The output variable's gradient.

        Raises
        ------

        NotImplementedError: Raised unless this method is overloaded.
        """


    def __call__(self, *variables: Variable) -> Variable:
        """Calls the function.

        Parameters
        ----------

        *variables: Several input variables.

        Returns
        -------

        variable: The output variable.
        """

        output = self.forward(*variables)


        # Overloading function.

        def backward(root: bool = True) -> None:

            self.backward(*variables, root + output.grad)  # Update gradients.

            for variable in variables:
                variable.backward(False)  # Recurse.

            output.grad *= output.leaf
        output.backward = backward

        return output