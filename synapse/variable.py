# Copyright (c) 2023 ethereal.
#
# This software is licensed under the MIT license. See LICENSE for details.


"""Synapse variables."""


import numpy as np


class Variable:
    """Defines a variable.

    Variables are used to keep track of values and their gradients during
    backward propagation.

    Attributes
    ----------

    value: A tensor containing the variable's value.

    gradient: A tensor containing the variable's gradient.

    external: A boolean indicating whether the variable is an external node.

    Methods
    -------

    backward: Performs backward propagation.
    """

    def __init__(self, value: np.array, external: bool = False) -> None:
        """Initializes the variable.

        Parameters
        ----------

        value: An initial value for the `value` attribute.

        external: An initial value for the `external` attribute.
        """

        self.value = np.array(value)
        self.gradient = np.zeros_like(value)
        self.external = external


    @staticmethod
    def backward(self, external: bool = False) -> None:
        """Performs backward propagation.

        Parameters
        ----------

        external: A boolean indicating whether the variable is an external node.
        """

        pass